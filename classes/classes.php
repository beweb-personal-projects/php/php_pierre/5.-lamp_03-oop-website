<?php
require('components.php');

abstract class Doctype {
    protected $doctype_start;
    protected $doctype_end;
    
    function __construct() {
        $this->doctype_start = "
            <!DOCTYPE html>
            <html lang='en'>
            <head>
                <meta charset='UTF-8'>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css' rel='stylesheet' integrity='sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3' crossorigin='anonymous'>
                <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js' integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous'></script>
                <title>{$this->getPage()}</title>
            </head>
            <body>
        ";

        $this->doctype_end = "
            </body>
            </html>
        ";
    }

    public function getDoctypeStart() {
        return $this->doctype_start;
    }

    public function getDoctypeEnd() {
        return $this->doctype_end;
    }

    public function getPage() {
        if(isset($_GET['page'])) {
            return ucfirst($_GET['page']);
        } else {
            return "Home";
        }
    }
}

class Website extends Doctype {
    protected $navbar;
    protected $content;
    protected $footer;

    function __construct() {
        parent::__construct();
        $this->navbar = new Navbar($this->generateNavItems());
        $this->content = new Content();
        $this->footer = new Footer();
    }

    public function rendering() {
        echo $this->getDoctypeStart();

        echo $this->getNavbar();
        include $this->getContentWebsite();
        echo $this->getFooter();

        echo $this->getDoctypeEnd();
    }

    // ---------------------------------------------------------------
    // ALL THE GETTERSSSSS

    public function getNavbar() {
        return $this->navbar->getNavbar();
    }

    public function getContentWebsite() {
        return $this->content->getContent();
    }

    public function getFooter() {
        return $this->footer->getFooter();
    }

    public function getContent() {
        $array_content = scandir('./content/');
        return array_splice($array_content, 2, count($array_content));
    }
    

    // ---------------------------------------------------------------
    // ALL THE SETTERSSSSS

    public function setTitle($current_page) {
        $this->title = $current_page;
    }

    // ---------------------------------------------------------------
    // OTHER METHODSSSS

    public function removeExtentionName($file) {
        $current_file = substr($file, 0, strrpos($file, "."));
        return $current_file;
    }

    public function generateNavItems() {
        $array_items = [];
        foreach ($this->getContent() as $i => $item) {
            array_push($array_items, $this->removeExtentionName(ucfirst($item)));
        }
        return $array_items;
    }

    public static function getURL() {
        return "http://" . $_SERVER["HTTP_HOST"];
    }
}
