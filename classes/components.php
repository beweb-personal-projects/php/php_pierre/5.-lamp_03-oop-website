<?php 

class Navbar {
    protected $navbar;

    function __construct($pages) {
        $this->navbar = "
            <nav class='navbar navbar-expand-lg navbar-dark bg-dark'>
                <div class='container-fluid'>
                    <a class='navbar-brand' href='#'>OOP WEBSITE</a>
                    <button class='navbar-toggler' type='button' data-bs-toggle='collapse' data-bs-target='#navbarNav' aria-controls='navbarNav' aria-expanded='false' aria-label='Toggle navigation'>
                        <span class='navbar-toggler-icon'></span>
                    </button>
                    <div class='collapse navbar-collapse' id='navbarNav'>
                        <ul class='navbar-nav'>
                            " . $this->getAllNavItems($pages) ."
                        </ul>
                    </div>
                </div>
            </nav>
        ";
    }

    function getNavbar() {
        return $this->navbar;
    }

    function getAllNavItems($pages) {
        $all_nav_items = "";

        foreach ($pages as $i => $item) {
            $all_nav_items .= "
                <li class='nav-item'>
                    <a class='nav-link' href='http://localhost:2100/?page=" . lcfirst($item) ."'>{$item}</a>
                </li>
            ";
        }

        return $all_nav_items;
    }

}

class Content {
    protected $content;

    function __construct() {
        $this->content = $this->currentContent();
    }

    function getContent() {
        return $this->content;
    }

    function currentContent() {
        if(isset($_GET['page'])) {
            return "./content/" . $_GET['page'] . ".php";
        } else {
            return "./content/home.php";
        }
    }
} 

class Footer {
    protected $footer;

    function __construct() {
        $this->footer = "
            <footer class='bg-dark text-center text-lg-start fixed-bottom'>
                <div class='text-center p-3 text-light'>
                    © 2022 Copyright:
                    <a class='text-light' href='http://localhost:2100/'>PDO TESTING WEBSITE</a>
                </div>
            </footer>
        ";
    }

    function getFooter() {
        return $this->footer;
    }
}